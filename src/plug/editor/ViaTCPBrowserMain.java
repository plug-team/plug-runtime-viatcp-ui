package plug.editor;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by Ciprian TEODOROV on 04/03/17.
 */
@Deprecated
public class ViaTCPBrowserMain extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("OBP UI");


        System.setProperty("http.proxyHost","proxy");
        System.setProperty("http.proxyPort","8080");

        System.setProperty("https.proxyHost","proxy");
        System.setProperty("https.proxyPort","8080");

        Scene s = new Scene(new ViaTCPBrowser(primaryStage), 800, 600);
        primaryStage.setScene(s);

        primaryStage.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });

        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}

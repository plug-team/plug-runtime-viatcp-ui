package plug.editor;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableSet;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import plug.core.IConfiguration;
import plug.core.RuntimeDescription;
import plug.core.execution.ExecutionOLD;
import plug.core.execution.ExecutionDescription;
import plug.explorer.Simulator;
import plug.language.viatcp.ViaTCPPlugin;
import plug.language.viatcp.runtime.ViaTCPRuntime;
import plug.simulation.ui.fx.SimulationView;

/**
 * Created by Ciprian TEODOROV on 04/03/17.
 */
@Deprecated
public class ViaTCPBrowser extends BorderPane {
    
    private Stage primaryStage;
    ConnectionData connectionData = new ConnectionData();
    protected final ViaTCPPlugin module = new ViaTCPPlugin();
    ViaTCPRuntime runtime;

    static class ConnectionData {
    	String ip = "127.0.0.1";
    	int port = 12346;
    }

    public ViaTCPBrowser(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.runtime = new ViaTCPRuntime(connectionData.ip, connectionData.port);

//        this.setTop(createToolBar(primaryStage));
//        this.getChildren().add(createMenuBar());
        
        ExecutionOLD execution = new ExecutionOLD("Simulation",
				new RuntimeDescription(module, () -> runtime),
				new ExecutionDescription("Simulation", Simulator::new)
			);
        execution.getOrInitContext();
        ObservableSet<IConfiguration> configurations = FXCollections.observableSet();
        this.setCenter(new SimulationView(execution.getContext(), configurations));
        
        
    }
    TextField ip;
    TextField port;
    ToolBar createToolBar(Stage stage) {
    	ip = new TextField("IP");
    	ip.setText(this.connectionData.ip);
    	ip.setFocusTraversable(true);
    	ip.focusedProperty().addListener(this::ipFocusLost);
    	
    	
    	port = new TextField("Port");
    	port.setText(Integer.toString(this.connectionData.port));
    	port.setFocusTraversable(true);
    	port.focusedProperty().addListener(this::ipFocusLost);
    	
    	
        Button simulateButton = new Button("Simulate");
        simulateButton.setOnAction(this::handleSimulationRequest);
        simulateButton.setTooltip(new Tooltip("Start simulation"));
        
        ToolBar toolBar = new ToolBar(ip, port,
        		simulateButton
        );
        return toolBar;
    }

    private void ipFocusLost(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
    	if (!newValue) {
    		connectionData.ip = ip.getText();
    	}
    }
    
    private void portFocusLost(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
    	if (!newValue) {
    		connectionData.port = Integer.parseInt(port.getText());
    	}
    }
    
    private MenuBar createMenuBar() {
        MenuBar bar = new MenuBar();
        Platform.runLater(() -> bar.setUseSystemMenuBar(true));
        Menu fileMenu = new Menu("Actions");

        MenuItem rootDirectory = new MenuItem("Simulate");
        rootDirectory.setOnAction(this::handleSimulationRequest);

        fileMenu.getItems().addAll(rootDirectory);
        bar.getMenus().add(fileMenu);

        return bar;
    }

    private void handleSimulationRequest (ActionEvent e) {
    	this.runtime = new ViaTCPRuntime(connectionData.ip, connectionData.port);

        thisScene = primaryStage.getScene();
        Button back = new Button("Back");
        back.setOnAction(evt -> {
            primaryStage.setScene(thisScene);
        });

		BorderPane pane = new BorderPane();
        pane.setTop(back);
		ExecutionOLD execution = new ExecutionOLD("Simulation",
				new RuntimeDescription(module, () -> runtime),
				new ExecutionDescription("Simulation", Simulator::new)
			);
        pane.setCenter(new SimulationView(execution.getContext(), FXCollections.emptyObservableSet()));

        Scene scene = new Scene(pane, 800, 600);
        primaryStage.setScene(scene);
    }

    Scene thisScene;
}
